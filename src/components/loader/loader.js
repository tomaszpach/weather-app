import React from 'react';

const loader = () => (
    <div className="spinner">
        <div className="dot1"/>
        <div className="dot2"/>
    </div>
);

export default loader;